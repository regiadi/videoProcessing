videoObject = VideoReader('xylophone.mpg');
get(videoObject)

nFrames = videoObject.NumberOfFrames;
vidHeight = videoObject.Height;
vidWidth = videoObject.Width;

singleFrame = zeros(vidHeight, vidWidth, 3, 'uint8');
grayscaleFrame = [];

% Preallocate movie structure.
mov(1:nFrames) = struct( ...
    'cdata', ...
    zeros(vidHeight, vidWidth, 3, 'uint8'), ...
    'colormap', ...
    [] ...
);

% Preallocate grayscale movie
grayscaleMovie(1:nFrames) = struct( ...
    'cdata', ...
    zeros(vidHeight, vidWidth, 3, 'uint8'), ...
    'colormap', ...
    [] ...
);

% Read one frame at a time.
for k = 1 : nFrames
    singleFrame = read(videoObject, k);
    mov(k).cdata = singleFrame;
    
    % add grayscale image into three channel frame
    for j = 1 : 3
        grayscaleFrame = uint8(rgb2gray(singleFrame));
        grayscaleMovie(k).cdata(:, :, j) = grayscaleFrame;
    end
end

% Size a figure based on the video's width and height.
hf = figure;
set(hf, 'position', [150 150 vidWidth vidHeight]);

% Play back the movie once at the video's frame rate.
%movie(hf, mov, 1, videoObject.FrameRate);
movie(hf, grayscaleMovie, 10, videoObject.FrameRate);

