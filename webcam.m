imaqhwinfo

% get webcam info
a = imaqhwinfo('winvideo', 1)

% list of webcam supported format
a.SupportedFormats

% get webcam
vid = videoinput('winvideo', 1, 'MJPG_320x240');

% open webcam preview window
preview(vid);

% storing a frame in a variable
data = getsnapshot(vid);
imshow(data);

% deleting the videoinput to avoid filling up memory
delete(a);
delete(v);